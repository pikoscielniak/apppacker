package main
import (
	"fmt"
	"os"
	"AppPacker/packer"
)

const helpMessage = "Unknown option or lack of it. Allowed options: <Path to config>"

func printHelp() {
	fmt.Println(helpMessage)
}

func main() {

	argLength := len(os.Args)
	if argLength < 2 {
		printHelp()
		return;
	}
	configPath := os.Args[1]

	fmt.Println("PACKING...")

	params, err := packer.ReadConfig(configPath)

	if err != nil {
		fmt.Println(err)
		return
	}

	err = packer.PackApps(params)

	if err != nil {
		fmt.Println(err)
		return
	}

	fmt.Println("APPPACKER SUCCESS")
}