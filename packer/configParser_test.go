package packer_test

import (
	"testing"
	. "github.com/smartystreets/goconvey/convey"
	"AppPacker/packer"
	"app_manager/utils"
)
const (
	configPath = `./configPath.json`
)

var appToPacksParams = packer.AppToPacks{
	DestinationDir:"./testAssets",
	Apps: []packer.AppToPack{
		{SourceDir:"./", AppId: "test1"},
		{SourceDir:"./", AppId: "test2"},
	},
}

func TestConfigParser(t *testing.T) {
	Convey("ConfigParser", t, func() {
		err := setup()

		So(err, ShouldBeNil)

		data, err := packer.ReadConfig(configPath)

		So(err, ShouldBeNil)

		So(data.Apps, ShouldHaveLength, 2)

		app0 := data.Apps[0]
		app1 := data.Apps[1]

		expectedApp0 := appToPacksParams.Apps[0]
		expectedApp1 := appToPacksParams.Apps[1]

		So(app0.SourceDir, ShouldEqual, expectedApp0.SourceDir)
		So(app0.AppId, ShouldEqual, expectedApp0.AppId)

		So(app1.SourceDir, ShouldEqual, expectedApp1.SourceDir)
		So(app1.AppId, ShouldEqual, expectedApp1.AppId)
	})
}

func setup() error {
	return utils.SaveJson(configPath, appToPacksParams)
}