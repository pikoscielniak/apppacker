package packer_test
import (
	"testing"
	. "github.com/smartystreets/goconvey/convey"
	"AppPacker/packer"
	"app_manager/utils"
	"app_manager/installer"
	"fmt"
)

func TestActions(t *testing.T) {
	Convey("AppPacker", t, func() {

		Convey("Packs app when folder is empty", func() {

			err := beforeActions()

			So(err, ShouldBeNil)

			err = packer.PackApps(appToPacksParams)

			So(err, ShouldBeNil)

			app0 := appToPacksParams.Apps[0]
			app0Path := getPath(app0.AppId)
			err = utils.Exists(app0Path)
			So(err, ShouldBeNil)

			app1 := appToPacksParams.Apps[1]
			app1Path := getPath(app1.AppId)
			err = utils.Exists(app1Path)
			So(err, ShouldBeNil)
		})

		SkipConvey("Packs app when folder is not empty", func() {

			fmt.Println("test works")
			appToPacksParams.DestinationDir = appToPacksParams.DestinationDir + "2"

			err := packer.PackApps(appToPacksParams)

			So(err, ShouldBeNil)

			app0 := appToPacksParams.Apps[0]
			app0Path := getPath(app0.AppId)
			err = utils.Exists(app0Path)
			So(err, ShouldBeNil)

			app1 := appToPacksParams.Apps[1]
			app1Path := getPath(app1.AppId)
			err = utils.Exists(app1Path)
			So(err, ShouldBeNil)
		})
	})
}

func getPath(appId string) string {
	return utils.Join(appToPacksParams.DestinationDir, installer.PackagesFolderName, appId, appId + ".zip")
}

func beforeActions() error {
	return utils.RemoveAll(appToPacksParams.DestinationDir)
}