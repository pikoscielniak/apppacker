package packer

type AppToPack struct {
	SourceDir      string `json:"z"`
	AppId string `json:"unikalnaNazwa"`
}

type AppToPacks struct {
	DestinationDir string `json:"sciezkaDocelowa"`
	Apps []AppToPack `json:"aplikacje"`
}