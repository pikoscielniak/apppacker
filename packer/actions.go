package packer
import (
	"app_manager/utils"
	"fmt"
	"app_manager/installer"
)

const (
	zipExt = ".zip"
)

func PackApps(params AppToPacks) error {
	fmt.Println("entering")
	for _, app := range params.Apps {
		fmt.Println("app", app.AppId)
		destPath := prepareDestPath(params.DestinationDir, app.AppId)
		err := utils.Exists(destPath)
		if err == nil {
			err = utils.Remove(destPath)
			if err != nil {
				return err
			}
		}

		parentDir := utils.ParentDir(destPath)
		err = utils.Exists(parentDir)
		if err != nil {
			err = utils.MkdirAllWithMode(parentDir, utils.Permissions)
			if err != nil {
				return err
			}
		}
		fmt.Println("ZIPPING source:", app.SourceDir, "destination:", destPath)

		err = utils.ZipDirectory(app.SourceDir, destPath)
		if err != nil {
			return err
		}
	}
	return nil
}

func prepareDestPath(destPath, appId string) string {
	return utils.Join(destPath, installer.PackagesFolderName, appId, appId + zipExt)
}
