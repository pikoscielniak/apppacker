package packer

import (
	"app_manager/utils"
	"encoding/json"
)

func ReadConfig(path string) (AppToPacks, error) {
	var config AppToPacks
	bytes, err := utils.ReadFile(path)
	if err != nil {
		return config, err
	}

	err = json.Unmarshal(bytes, &config)

	return config, err;
}
